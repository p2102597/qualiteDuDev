#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Notepad.h"

class Notepad : public QMainWindow
{
    Q_OBJECT

public:
    Notepad(QWidget *parent = nullptr);
    ~Notepad();

private:
    bool italic = false;
    bool underligne = false;
    bool bold = false;

    Ui::NotepadClass ui;
    QString currentFile;
    bool textModified = false;

    void save();
    void getSavefile();


private slots:
    void on_actionNew_triggered();
    void on_actionSave_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_as_triggered();
    void on_actionExit_triggered();
    void on_actionBold_triggered();
    void on_actionitalic_triggered();
    void on_actionuderligne_triggered();
    void on_actioninfo_triggered();


    void textChanged();
    void copie();
    void paste();
    void cut();

    void color();
    void font();

    void undo();
    void redo();
};