#include "Notepad.h"
#include <iostream>
#include <QFileDialog>
#include <QMessageBox>
#include <fstream>
#include <QFontDialog>
#include <QColorDialog>

Notepad::Notepad(QWidget *parent): QMainWindow(parent)
{

    ui.setupUi(this);
    connect(ui.textEdit, &QTextEdit::textChanged, this, &Notepad::textChanged);

    connect(ui.actionCopy, &QAction::triggered, this, &Notepad::copie);
    connect(ui.actionPaste, &QAction::triggered, this, &Notepad::paste);
    connect(ui.actionCut, &QAction::triggered, this, &Notepad::cut);

    

    connect(ui.actionColor, &QAction::triggered, this, &Notepad::color);
    connect(ui.actionFont, &QAction::triggered, this, &Notepad::font);

    connect(ui.actionUndo, &QAction::triggered, this, &Notepad::undo);
    connect(ui.actionRedo, &QAction::triggered, this, &Notepad::redo);
}

Notepad::~Notepad()
{}

void Notepad::on_actionNew_triggered()
{
    currentFile.clear();
    ui.textEdit->setText(QString());
}

void Notepad::textChanged() {
    textModified = true;
    if (currentFile.isEmpty()) setWindowTitle("Notepad *");
    else {
        setWindowTitle(currentFile + " *");
    }
    
}

void Notepad::paste()
{
    ui.textEdit->paste();
}
void Notepad::cut()
{
    ui.textEdit->cut();
}
void Notepad::color()
{
    QColor colorSelected = Qt::red;
    QColor color = QColorDialog::getColor(colorSelected, this);
    if (color.isValid())
        ui.textEdit->setTextColor(color);
}
void Notepad::font()
{
    bool fontSelected;
    QFont font = QFontDialog::getFont(&fontSelected, this);
    if (fontSelected)
        ui.textEdit->setCurrentFont(font);
    
}
void Notepad::undo()
{
    ui.textEdit->undo();
}
void Notepad::redo()
{
    ui.textEdit->redo();
}
void Notepad::copie()
{
    ui.textEdit->copy();
}

void Notepad::save() {
    textModified = false;
    QFile file(currentFile);
    setWindowTitle(currentFile);
    if (!file.open(QIODevice::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot open file: " +
            file.errorString());
        return;
    }

    QTextStream out(&file);
    QString text = ui.textEdit->toPlainText();
    ui.textEdit->setText(text);
    out << text;
    file.close();
}

void Notepad::getSavefile()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save the file");
    if (fileName.isEmpty())
        return;
    currentFile = fileName;
    setWindowTitle(currentFile);
}

void Notepad::on_actionSave_triggered()
{
    if (currentFile.isEmpty())
    {
        getSavefile();
    }
    Notepad::save();
}

void Notepad::on_actionSave_as_triggered()
{
    if (currentFile.isEmpty()) {
        getSavefile();
    }
    getSavefile();
    Notepad::save();
}

void Notepad::on_actionOpen_triggered()
{
    textModified = false;
    QString fileName = QFileDialog::getOpenFileName(this, "Open the file");
    if (fileName.isEmpty())
        return;
    QFile file(fileName);
    currentFile = fileName;
    if (!file.open(QIODevice::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot open file: " +
            file.errorString());
        return;
    }
    setWindowTitle(fileName);
    QTextStream in(&file);
    QString text = in.readAll();
    ui.textEdit->setText(text);
    file.close();
}
void Notepad::on_actionExit_triggered() {
    if (textModified) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Question?", "Voulez-vous sauvegarder ?", QMessageBox::Yes | QMessageBox::No);
        if (reply== QMessageBox::Yes)
        {
            on_actionSave_triggered();
        }
    }
    
    this->close();
}

void Notepad::on_actionBold_triggered()
{
    bold = !bold;
    if (bold) { ui.textEdit->setFontWeight(QFont::Bold); }
    else { ui.textEdit->setFontWeight(QFont::Normal); }
}

void Notepad::on_actionitalic_triggered()
{
    italic = !italic;
    ui.textEdit->setFontItalic(italic);
}

void Notepad::on_actionuderligne_triggered()
{
    underligne = !underligne;
    ui.textEdit->setFontUnderline(underligne);
}

void Notepad::on_actioninfo_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("The Notepad example demonstrates how to code a basic text editor using QtWidgets");
    msgBox.exec();
}
