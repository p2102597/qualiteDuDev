#pragma once
#include <QtCharts/qchart.h>
#include "Data.h"
class AbstractChartView
{
protected:
	Data db;
public:
	virtual QWidget* getChartView() = 0;

};

