#pragma once
#include <QString>

class Passenger
{
protected:
	QString iden;
	int classe;
	int age;
	int sex;
	int survived;
public:

	Passenger(QString iden, int classe, int age, int sex, int survived);
	QString toString();
	int getsex();
	int getClasse();
	bool getSurvived();
	bool getAge();
};

