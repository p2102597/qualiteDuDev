#include "Data.h"
#include<iostream>

void Data::affiche()
{
	for (Passenger pas : listePassenger) {
		qDebug() << pas.toString();
	}
	
	
}

void Data::load(QString nameFile)
{
	QFile file(nameFile);
	file.open(QFile::ReadOnly);
	QTextStream steam(&file);

	for (size_t i = 0; i < 5; i++)
	{
		steam.readLine();
	}

	QString iden;
	int classe, age, sex, survived;

	while (!steam.atEnd())
	{
		steam >> iden >> classe >> age >> sex >> survived;
		Passenger tmp(iden, classe, age, sex, survived);
		listePassenger.append(tmp);
	}
}

int Data::nbrF()
{
	int cpt = 0;
	for (Passenger pas : listePassenger) {
		if (pas.getsex() == 0)
		{
			cpt++;
		}
	}
	return cpt;
}

int Data::nbrH()
{
	int cpt = 0;
	for (Passenger pas : listePassenger) {
		if (pas.getsex() == 1)
		{
			cpt++;
		}
	}
	return cpt;
}

int Data::nbrPersonne(int classe, bool survie)
{
	int cpt = 0;
	
	for (Passenger p : listePassenger) {
		if (p.getClasse() == classe and p.getSurvived() == survie) {
			cpt++;
		}
	}
	return cpt;
}

int Data::nbrAdulte(bool survie)
{
	int cpt = 0;
	for (Passenger p : listePassenger) {
		if (p.getAge() == 1 and p.getSurvived() == survie) {
			cpt++;
		}
	}
	return cpt;
}

int Data::nbrChild(bool survie)
{
	int cpt = 0;
	for (Passenger p : listePassenger) {
		if (p.getAge() == 0 and p.getSurvived() == survie) {
			cpt++;
		}
	}
	return cpt;
}

QVector<Passenger> Data::getPassenger()
{
	return QVector<Passenger>();
}
