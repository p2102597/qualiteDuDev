#pragma once
#include "AbstractChartView.h"
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>

class BarChartView :
    public AbstractChartView
{
public:
    BarChartView(Data* data);
    QWidget* getChartView() override;
};

