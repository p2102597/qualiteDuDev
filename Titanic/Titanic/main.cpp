#include "Titanic.h"
#include <QtWidgets/QApplication>

#include "Data.h"
#include "AbstractChartView.h"
#include "PieChartView.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Titanic w("titanic.dbf");
    w.show();

    return a.exec();
}
