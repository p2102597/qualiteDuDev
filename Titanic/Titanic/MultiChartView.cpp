#include "MultiChartView.h"

MultiChartView::MultiChartView(Data* data) {
	this->db = *data;
}

QWidget* MultiChartView::getChartView()
{
	QWidget *central = new QWidget();
	QGridLayout* centralLayout = new QGridLayout();
	central->setLayout(centralLayout);


	// graphique 1
	QBarSet* set0 = new QBarSet("Population");
	*set0 << db.nbrPersonne(0, false) + db.nbrPersonne(0, true) << db.nbrPersonne(1, false) + db.nbrPersonne(1, true) << db.nbrPersonne(2, false) + db.nbrPersonne(2, true) << db.nbrPersonne(3, false) + db.nbrPersonne(3, true);
	QHorizontalBarSeries* series = new QHorizontalBarSeries();
	series->append(set0);

	QChart* chart = new QChart();
	chart->addSeries(series);
	chart->setTitle("Population/classe");
	chart->setAnimationOptions(QChart::SeriesAnimations);

	QStringList categories;
	categories << "Equipage" << "Premiere" << "Deuxieme" << "Troisieme";
	QBarCategoryAxis* axisY = new QBarCategoryAxis();
	axisY->append(categories);
	chart->addAxis(axisY, Qt::AlignLeft);
	series->attachAxis(axisY);
	QValueAxis* axisX = new QValueAxis();
	chart->addAxis(axisX, Qt::AlignBottom);
	series->attachAxis(axisX);
	axisX->applyNiceNumbers();

	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);

	QChartView* chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);



	// graphique 2
	QBarSet* G2set0 = new QBarSet("Peri");
	QBarSet* G2set1 = new QBarSet("Survecu");

	*G2set0 << db.nbrPersonne(0, false) << db.nbrPersonne(1, false) << db.nbrPersonne(2, false) << db.nbrPersonne(3, false);
	*G2set1 << db.nbrPersonne(0, true) << db.nbrPersonne(1, true) << db.nbrPersonne(2, true) << db.nbrPersonne(3, true);

	QHorizontalStackedBarSeries* G2series = new QHorizontalStackedBarSeries();
	G2series->append(G2set0);
	G2series->append(G2set1);

	QChart* G2chart = new QChart();
	G2chart->addSeries(G2series);
	G2chart->setTitle("Survecu/Categorie");
	G2chart->setAnimationOptions(QChart::SeriesAnimations);

	QBarCategoryAxis* G2axisY = new QBarCategoryAxis();
	G2axisY->append(categories);
	G2chart->addAxis(G2axisY, Qt::AlignLeft);
	G2series->attachAxis(G2axisY);
	QValueAxis* G2axisX = new QValueAxis();
	G2chart->addAxis(G2axisX, Qt::AlignBottom);
	G2series->attachAxis(G2axisX);

	G2chart->legend()->setVisible(true);
	G2chart->legend()->setAlignment(Qt::AlignBottom);

	QChartView* chartView2 = new QChartView(G2chart);
	chartView2->setRenderHint(QPainter::Antialiasing);


	// graphique 3

	QBarSet* g3set0 = new QBarSet("Population");
	*g3set0 << db.nbrAdulte(true)+ db.nbrAdulte(false) << db.nbrChild(true)+ db.nbrChild(false);
	QHorizontalBarSeries* g3series = new QHorizontalBarSeries();
	g3series->append(g3set0);

	QChart* g3chart = new QChart();
	g3chart->addSeries(g3series);
	g3chart->setTitle("Population/Age");
	g3chart->setAnimationOptions(QChart::SeriesAnimations);

	QStringList g3categories;
	g3categories << "Adulte" << "Enfant";
	QBarCategoryAxis* g3axisY = new QBarCategoryAxis();
	g3axisY->append(g3categories);
	g3chart->addAxis(g3axisY, Qt::AlignLeft);
	g3series->attachAxis(g3axisY);
	QValueAxis* g3axisX = new QValueAxis();
	g3chart->addAxis(g3axisX, Qt::AlignBottom);
	g3series->attachAxis(g3axisX);
	g3axisX->applyNiceNumbers();

	g3chart->legend()->setVisible(true);
	g3chart->legend()->setAlignment(Qt::AlignBottom);

	QChartView* chartView3 = new QChartView(g3chart);
	chartView3->setRenderHint(QPainter::Antialiasing);


	// graphique 4
	QBarSet* G4set0 = new QBarSet("Peri");
	QBarSet* G4set1 = new QBarSet("Survecu");

	*G4set0 << db.nbrAdulte(false) << db.nbrChild(false) ;
	*G4set1 << db.nbrAdulte(true) << db.nbrChild(true);

	QHorizontalStackedBarSeries* G4series = new QHorizontalStackedBarSeries();
	G4series->append(G4set0);
	G4series->append(G4set1);

	QChart* G4chart = new QChart();
	G4chart->addSeries(G4series);
	G4chart->setTitle("Survecu/Age");
	G4chart->setAnimationOptions(QChart::SeriesAnimations);

	QBarCategoryAxis* G4axisY = new QBarCategoryAxis();
	G4axisY->append(g3categories);
	G4chart->addAxis(G4axisY, Qt::AlignLeft);
	G4series->attachAxis(G4axisY);
	QValueAxis* G4axisX = new QValueAxis();
	G4chart->addAxis(G4axisX, Qt::AlignBottom);
	G4series->attachAxis(G4axisX);

	G4chart->legend()->setVisible(true);
	G4chart->legend()->setAlignment(Qt::AlignBottom);

	QChartView* chartView4 = new QChartView(G4chart);
	chartView4->setRenderHint(QPainter::Antialiasing);

	
	centralLayout->addWidget(chartView,1,1);
	centralLayout->addWidget(chartView2,1,2);
	centralLayout->addWidget(chartView3, 2, 1);
	centralLayout->addWidget(chartView4, 2, 2);






	return central;
}
