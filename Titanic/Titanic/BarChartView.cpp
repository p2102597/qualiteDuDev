#include "BarChartView.h"

BarChartView::BarChartView(Data* data)
{
	this->db = *data;
}

QWidget* BarChartView::getChartView()
{
	QBarSet* set0 = new QBarSet("Peri");
	QBarSet* set1 = new QBarSet("Survecu");
	*set0 << db.nbrPersonne(0,false) << db.nbrPersonne(1, false) << db.nbrPersonne(2, false) << db.nbrPersonne(3, false);
	*set1 << db.nbrPersonne(0, true) << db.nbrPersonne(1, true) << db.nbrPersonne(2, true) << db.nbrPersonne(3, true);
		
	QBarSeries* series = new QBarSeries();
	series->append(set0);
	series->append(set1);

	QChart* chart = new QChart();
	chart->addSeries(series);
	chart->setTitle("Categorie");
	chart->setAnimationOptions(QChart::SeriesAnimations);

	QStringList categories;
	categories << "Equipage" << "Premiere" << "Deuxieme" << "Troisieme";
	QBarCategoryAxis* axisX = new QBarCategoryAxis();
	axisX->append(categories);
	chart->addAxis(axisX, Qt::AlignBottom);
	series->attachAxis(axisX);

	QValueAxis* axisY = new QValueAxis();
	chart->addAxis(axisY, Qt::AlignLeft);
	series->attachAxis(axisY);


	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);

	QChartView* chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);
	return chartView;
}
