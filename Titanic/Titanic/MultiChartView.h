#pragma once
#include "AbstractChartView.h"
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>
#include <QtCharts/QHorizontalBarSeries>
#include <QGridLayout>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QHorizontalStackedBarSeries>

class MultiChartView :
    public AbstractChartView
{
    MultiChartView();


public:
    MultiChartView(Data* data);
    QWidget* getChartView() override;
};

