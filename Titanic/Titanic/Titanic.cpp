#include "Titanic.h"

Titanic::Titanic(QString name,QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    db.load(name);

}

Titanic::~Titanic()
{}

void Titanic::on_actionration_H_F_triggered()
{
    ChartView = new PieChartView(&db);
    this->setCentralWidget(ChartView->getChartView());
}

void Titanic::on_actionCategorie_triggered()
{
    ChartView = new BarChartView(&db);
    this->setCentralWidget(ChartView->getChartView());
}

void Titanic::on_actionAge_categorie_triggered()
{
    ChartView = new MultiChartView(&db);
    this->setCentralWidget(ChartView->getChartView());
}
