#include "Passenger.h"

Passenger::Passenger(QString iden, int classe, int age, int sex, int survived)
{
	this->iden = iden;
	this->classe = classe;
	this->age = age;
	this->sex = sex;
	this->survived = survived;
}

QString Passenger::toString()
{
	return iden + " " + QString::number(classe) + " " + QString::number(age) + " " + QString::number(sex) + " " + QString::number(survived);
}

int Passenger::getsex()
{
	return sex;
}

int Passenger::getClasse()
{
	return classe;
}

bool Passenger::getSurvived()
{
	return survived;
}

bool Passenger::getAge()
{
	return age;
}
