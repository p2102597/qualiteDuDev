#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Titanic.h"

#include "Data.h"
#include "AbstractChartView.h"
#include "PieChartView.h"
#include "BarChartView.h"
#include "MultiChartView.h"

class Titanic : public QMainWindow
{
    Q_OBJECT

public:
    Titanic(QString name,QWidget *parent = nullptr);
    ~Titanic();
private:
    Ui::TitanicClass ui;
    Data db;
    AbstractChartView* ChartView;

private slots:
    void on_actionration_H_F_triggered();
    void on_actionCategorie_triggered();
    void on_actionAge_categorie_triggered();
};
