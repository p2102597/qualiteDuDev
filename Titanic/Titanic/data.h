#pragma once
#include <Qvector>
#include "Passenger.h"
#include <Qdir>
#include <QDebug>

class Data
{
private:
	QVector<Passenger> listePassenger;
public:
	void affiche();
	void load(QString nameFile);
	int nbrF();
	int nbrH();
	int nbrPersonne(int classe, bool survie);
	int nbrAdulte(bool survie);
	int nbrChild(bool survie);
	QVector<Passenger> getPassenger();
};