#include "PieChartView.h"

PieChartView::PieChartView(Data *data) {
	this->db = *data;
}

QWidget* PieChartView::getChartView()
{
	QPieSeries *series;
	series = new QPieSeries();
	series->append("Homme", db.nbrH());
	series->append("Femme", db.nbrF());

	QChart* chart = new QChart();
	chart->addSeries(series);
	chart->setTitle("Propotion H/F");
	chart->setAnimationOptions(QChart::SeriesAnimations);
	chart->legend()->hide();

	QChartView* chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	return chartView;
}
