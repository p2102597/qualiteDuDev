#pragma once
#include "AbstractChartView.h"
#include <QtCharts/qpieseries.h>
#include <QtCharts/qpieseries.h>
#include <QtCharts/QChartView>

class PieChartView: public AbstractChartView
{

public:
    PieChartView(Data* data);
    QWidget* getChartView() override;
};

