#pragma once
#include "promotion.h"
#include <qwidget.h>
#include <qcombobox.h>
#include "controllers.h"
#include <qlineedit.h>

/**
 * @brief ViewForms est une classe qui herite de QObject et qui a pour but la vie de formulaire
*/
class ViewForms : public QObject
{
	Q_OBJECT;
private:
	Promotion* promo;
	QWidget* Form;
	Abstract_Controller* controllerDelete;
	Abstract_Controller* controllerAdd;
public:
	ViewForms(Promotion* promo, QWidget* Form);
public slots:
	void remove();
	void add();
};

