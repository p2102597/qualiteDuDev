#pragma once
#include "promotion.h"
#include <qwidget.h>

/**
 * @brief la vue du digramme en camembert
 * @author 
*/
class ViewPieChart : public Observer
{
private:
	Promotion* promo;
	QWidget* graph;
	QLayout* centralLayout;

public:
	ViewPieChart(Promotion* promo, QWidget* graph);

	virtual void update() override;
};

