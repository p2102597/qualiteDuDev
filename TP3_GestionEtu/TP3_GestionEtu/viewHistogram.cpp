#include "viewHistogram.h"
#include <qgroupbox.h>
#include <qlayout.h>

/**
 * @brief constructeur
 * @param promo la promo qui sert de data pour faire l'hitogramme
 * @param graph la partie de l'interface ou il y aura l'hitogramme
*/
ViewHistogram::ViewHistogram(Promotion* promo, QWidget* graph): promo(promo), graph(graph)
{
	centralLayout = new QGridLayout();
	graph->findChild<QGroupBox*>("groupBox_bacs")->setLayout(centralLayout);
	this->update();
	promo->addObserver(this);
}

void ViewHistogram::update()
{
    ViewHistogram::clearLayout(centralLayout);
	centralLayout->addWidget(promo->getBarChartView());
}

/**
 * @brief permet de mettre a vide la layout
 * @param layout la layout a vider
*/
void ViewHistogram::clearLayout(QLayout* layout) {
    if (layout == NULL)
        return;
    QLayoutItem* item;
    while ((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
}