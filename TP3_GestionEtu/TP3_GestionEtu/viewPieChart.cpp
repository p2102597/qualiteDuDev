#include "viewPieChart.h"
#include <qgroupbox.h>
#include <qlayout.h>
#include "viewHistogram.h"

/**
 * @brief contructeur
 * @param promo les donnees pour fair le diagramme
 * @param graph la partie de l'interface ou on vas mettre le diagramme
*/
ViewPieChart::ViewPieChart(Promotion* promo, QWidget* graph) : promo(promo), graph(graph)
{
	centralLayout = new QGridLayout();
	graph->findChild<QGroupBox*>("groupBox_department")->setLayout(centralLayout);
	this->update();
	promo->addObserver(this);
}

void ViewPieChart::update()
{
	ViewHistogram::clearLayout(centralLayout);
	centralLayout->addWidget(promo->getPieChartView());
}