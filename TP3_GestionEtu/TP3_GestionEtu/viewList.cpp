#include "viewList.h"

/**
 * @brief suppression des eleves s�l�ction�
*/
void ViewList::remove()
{
	QList<QListWidgetItem*> listWi = listWidget->selectedItems();
	QStringList liste;
	for (size_t i = 0; i < listWi.size(); i++)
	{
		liste.append(listWi[i]->text());
	}
	controllerDelete->control(liste);
}

/**
 * @brief contructeur
 * @param promo les donn�es a afficher
 * @param listWidget une partie de l'interface ou afficher la liste
*/
ViewList::ViewList(Promotion* promo, QListWidget* listWidget)
{
	QDialog(parent);
	this->promo = promo;
	this->listWidget = listWidget;

	this->update();
	promo->addObserver(this);
	controllerDelete = new Controller_DeleteList(promo);
}


void ViewList::update()
{
	listWidget->clear();
	QList<Student> liste = *promo->getQList();
	for (Student& s : liste) {

		listWidget->addItem(s.toString());
		
	}
}
