#pragma once

/**
 * @brief permet le design parterne observeur
*/
class Observer {
public:
  virtual ~Observer() {}
  /**
   * @brief permet de mettre a jour la vue
  */
  virtual void update() = 0;
};

/**
 * @brief permet le design parterne observeur
*/
class Observable {
public:
  virtual ~Observable() {}
  virtual void addObserver(Observer* observer) = 0;
  virtual void removeObserver(Observer* observer) = 0;
  virtual void notifyObserver()  = 0;
};
