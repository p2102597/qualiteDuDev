#pragma once
#include "promotion.h"
#include <qwidget.h>


/**
 * @brief vue Histograme
*/
class ViewHistogram : public Observer
{
private :
	Promotion* promo;
	QWidget* graph;
	QLayout* centralLayout;
public:
	void static clearLayout(QLayout* layout);
	ViewHistogram(Promotion* promo, QWidget* graph);

	virtual void update() override;
};

