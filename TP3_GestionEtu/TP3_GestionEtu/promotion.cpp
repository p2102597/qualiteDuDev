#include "promotion.h"
#include "qfile.h";
#include "qtextstream.h";

#include "qdebug.h";

/**
 * @brief contructeur
*/
Promotion::Promotion()
{
}

/**
 * @brief permet de charger la classe
 * @param fileName le chemain du fichier a load
*/
void Promotion::loadFileCSV(QString fileName)
{
	QFile file(fileName);
	file.open(QFile::ReadOnly);
	QTextStream steam(&file);



	while (!steam.atEnd())
	{
		QString ligne = steam.readLine();

		QStringList listeDeMots = ligne.split(";");

		Student tmp(listeDeMots[0].toInt(), listeDeMots[1], listeDeMots[2], listeDeMots[4], listeDeMots[3]);
		liste.append(tmp);
	}
}

/**
 * @brief affiche le contenue de la promotion
*/
void Promotion::affiche()
{
	for (Student &s : liste)
	{
		qDebug() << s.getNumero() << " " << s.getNom() << " " << s.getPrenom();
	}
}

/**
 * @brief permet d'ajouter un Student
 * @param stu un etudient de la class Student
*/
void Promotion::addStudent(Student* stu)
{
	this->liste.append(*stu);
}

/**
 * @brief permet de supprimer un etudient en fonction de sont numero
 * @param i numero de l'etudient a supprimer
*/
void Promotion::removeStudent(int i)
{
	for (size_t in = 0; in < liste.length(); in++)
	{
		if (liste[in].getNumero() == i) {
			liste.remove(in);
		}
	}
}

/**
 * @brief permet de trouver un etudient
 * @param id le numero de l'etudient
 * @return retourne nullptr su il n'existe pas et le student alors
*/
Student* Promotion::find(int id)
{
	for (Student& s : liste) {
		if (id == s.getNumero()) {
			return &s;
		}
	}
	return nullptr;
}
/**
 * @brief genere l'hitogramme
 * @return un QWidget de l'hitogramme generer
*/
QWidget* Promotion::getBarChartView()
{
	QList<QBarSet*> set;
	QList<QString> bac;
	QList<int> NbrBac;
	for (Student& stu : liste) {
		bool isExiste = false;
		for (QString& str : bac) {
			if (str == stu.getBacOrigine()) {
				isExiste = true;
			}
		}
		if (!isExiste) {
			bac.append(stu.getBacOrigine());
			set.append(new QBarSet(stu.getBacOrigine()));
			NbrBac.append(0);
		}
	}
	for (Student& stu : liste) {
		for (size_t i = 0; i < bac.length(); i++)
		{
			if (stu.getBacOrigine() == bac[i]) {
				NbrBac[i]++;
			}
		}	
	}
	QBarSeries* series = new QBarSeries();
	for (size_t i = 0; i < set.length(); i++)
	{

		*set[i] << NbrBac[i];
		series->append(set[i]);
	}

	QChart* chart = new QChart();
	chart->addSeries(series);
	chart->setTitle("Series de bac");
	chart->setAnimationOptions(QChart::SeriesAnimations);

	QStringList categories;
	categories << "Bac" ;
	QBarCategoryAxis* axisX = new QBarCategoryAxis();
	axisX->append(categories);
	chart->addAxis(axisX, Qt::AlignBottom);
	series->attachAxis(axisX);

	QValueAxis* axisY = new QValueAxis();
	chart->addAxis(axisY, Qt::AlignLeft);
	series->attachAxis(axisY);

	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);

	QChartView* chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	return chartView;
}
/**
 * @brief genere un digramme en camembert
 * @return un QWidget de l'hitogramme generer
*/
QWidget* Promotion::getPieChartView()
{
	QMap<QString, int> depMap;
	for (Student& stu : liste) {
		if (!depMap.contains(stu.getDep()) or depMap.isEmpty()) {
			depMap.insert(stu.getDep(), 1);
		}
		else {
			depMap[depMap.find(stu.getDep()).key()]++;
		}
	}
	QPieSeries* series = new QPieSeries();
	for (auto key : depMap.keys()) {
		if (key == "0") {
			series->append("autre", depMap.value(key));
		}
		else {
			series->append(key, depMap.value(key));
		}
		
	}

	for (auto key : series->slices()) {
		key->setLabelVisible();
	}


	QChart* chart = new QChart();
	chart->addSeries(series);
	chart->setTitle("Repartition Geographique");
	chart->legend()->hide();

	QChartView* chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	return chartView;
}


/**
 * @brief ajouter un observer
 * @param observer 
*/
void Promotion::addObserver(Observer* observer)
{
	this->obs.append(observer);
}
/**
 * @brief supprime un observeur
 * @param observer 
*/
void Promotion::removeObserver(Observer* observer)
{
	this->obs.removeAll(observer);
}
/**
 * @brief update tous les observers
*/
void Promotion::notifyObserver()
{
	for (Observer*& o : obs) {
		o->update();
	}
}
