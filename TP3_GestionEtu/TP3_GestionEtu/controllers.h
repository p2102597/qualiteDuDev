#pragma once
#include <QStringList>
#include <QMessageBox>
#include "promotion.h"

/**
 * @brief AbstractController gere les listes étudients
*/
class Abstract_Controller
{
public:
  virtual ~Abstract_Controller() {}
  virtual void control(const QStringList& list) = 0;
};

/**
 * @brief Controller pour supprimer un etudient avec le boutton de la liste
*/
class Controller_DeleteList : public Abstract_Controller
{
private:
  Promotion* promo;
public:
    /**
    * @brief contructeur
    */
  Controller_DeleteList(Promotion* promo) : promo(promo) {}

  /**
  * @brief Control gere la suppresion de le ou les etudient selectionné 
  */
  virtual void control(const QStringList& list) override
  {
    for (QString student: list)
    {
        QStringList ligne = student.split(" ");
        promo->removeStudent(ligne[0].toInt());
    }
    promo->notifyObserver();
  }
};

/**
 * @brief Controller pour ajouter un etudient avec le boutton ajouter du formulaire
*/
class Controller_AddForm : public Abstract_Controller
{
private:
    Promotion* promo;
public:
    /**
    * @brief Constructor
    */
    Controller_AddForm(Promotion* promo) : promo(promo) {}

    /**
    * @brief Gere le processus d'ajout d'etudient
    */
    virtual void control(const QStringList& list) override
    {
        if (list.size() != 5) return;

        if (promo->find(list[0].toInt()) != nullptr)
        {
            QMessageBox::warning(nullptr,"Add Student","The student already exist.");
            return;
        }

        if (list[0] == "")
        {
            QMessageBox::warning(nullptr, "Add Student", "The card ID is empty."); return;
        }
        if (list[1] == "")
        {
            QMessageBox::warning(nullptr, "Add Student", "The firstname is empty."); return;
        }
        if (list[2] == "")
        {
            QMessageBox::warning(nullptr, "Add Student", "The lastname is empty."); return;
        }
        if (list[3] == "")
        {
            QMessageBox::warning(nullptr, "Add Student", "The bac is empty."); return;
        }
        if (list[4] == "")
        {
            QMessageBox::warning(nullptr, "Add Student", "The department is empty."); return;
        }

        Student* studentNew = new Student(list[0].toInt(), list[1], list[2], list[3], list[4]);
        promo->addStudent(studentNew);
        promo->notifyObserver();
    }
};

/**
 * @brief Controller suppression d'un etudient avec le formulaire 
*/
class Controller_DeleteForm : public Abstract_Controller
{
private:
    Promotion* promo;
public:
    /**
    * @brief Constructeur
    */
    Controller_DeleteForm(Promotion* promo) : promo(promo) {}

    /**
    * @brief gere le processus de suppression d'un etudient avec le formulaire
    */
    virtual void control(const QStringList& list) override
    {
        if (list.size() < 1) return;
        for (QString student : list)
        {
            promo->removeStudent(student.toInt());
        }
        promo->notifyObserver();
    }
};
