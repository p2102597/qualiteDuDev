#pragma once
#include "qstring.h"
/**
 * @brief Etudient des promotions
*/
class Student
{
private:
    int numero;
    QString nom, prenom, bacOrigine, dep;

public:
    Student();
    Student(int num, QString nom, QString prenom, QString bacOrigine, QString dep);

    QString getNom() {
        return nom;
    }
    QString getPrenom() {
        return prenom;
    }
    QString getBacOrigine() {
        return bacOrigine;
    }
    int getNumero() {
        return numero;
    }
    QString getDep() {
        return dep;
    }
    QString toString() {
        return QString::number(numero) + " - " + nom + " " + prenom + " " + bacOrigine + " (" + dep + ")";
    }
};

