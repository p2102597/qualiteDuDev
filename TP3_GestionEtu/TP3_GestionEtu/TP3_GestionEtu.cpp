#include "TP3_GestionEtu.h"

/**
     * @brief contructeur
     * @param promo mettre la promo qui vas etre utilis� dans l'application en tant que data
     * @param parent
    */
TP3_GestionEtu::TP3_GestionEtu(Promotion* promo, QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    listView = new ViewList(promo, ui.listWidget);
    listForm = new ViewForms(promo, ui.widget);
    histogramView = new ViewHistogram(promo, ui.widget);
    pieChartView = new ViewPieChart(promo, ui.widget);

    connect(ui.pushButton_delete_list, &QPushButton::clicked, listView, &ViewList::remove);
    connect(ui.pushButton_delete_number, &QPushButton::clicked,listForm, &ViewForms::remove);
    connect(ui.pushButton_addStudent, &QPushButton::clicked, listForm, &ViewForms::add);



}
