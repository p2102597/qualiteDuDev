#include "student.h"

Student::Student()
{
}
/**
 * @brief contructeur
 * @param num numero etudient
 * @param nom nom de l'etudient
 * @param prenom prenom de l'etudient
 * @param bacOrigine bac d'origine de l'etudient
 * @param dep departement d'origine de l'etudient
*/
Student::Student(int num, QString nom, QString prenom, QString bacOrigine, QString dep)
{
	this->numero = num;
	this->nom = nom;
	this->prenom = prenom;
	this->bacOrigine = bacOrigine;
	this->dep = dep;
}
