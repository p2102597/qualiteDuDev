#include "viewForms.h"

/**
 * @brief Constructeur de la viue des Formulaires
 * @param promo la promotion ou on peut ajouter et supprimer les eleves
 * @param Form Une partie de l'interface ou il y a les formulaires
*/
ViewForms::ViewForms(Promotion* promo, QWidget* Form) : promo(promo), Form(Form)
{
	controllerDelete = new Controller_DeleteForm(promo);
	controllerAdd = new Controller_AddForm(promo);

	Form->findChild<QComboBox*>("lineEdit_add_BAC")->addItem("S");
	Form->findChild<QComboBox*>("lineEdit_add_BAC")->addItem("L");
	Form->findChild<QComboBox*>("lineEdit_add_BAC")->addItem("ES");
	Form->findChild<QComboBox*>("lineEdit_add_BAC")->addItem("STI");
	
	for (size_t i = 1; i <= 95; i++)
	{
		Form->findChild<QComboBox*>("lineEdit_add_department")->addItem(QString::number(i));
	}for (size_t i = 1; i <= 6; i++)
	{
		int y = 970 + i;
		Form->findChild<QComboBox*>("lineEdit_add_department")->addItem(QString::number(y));
	}

}
/**
 * @brief ajouter un eleve dans la promo
*/
void ViewForms::add()
{
	QStringList list;
	list.append(Form->findChild<QLineEdit*>("lineEdit_add_number")->text());
	list.append(Form->findChild<QLineEdit*>("lineEdit_add_firstname")->text());
	list.append(Form->findChild<QLineEdit*>("lineEdit_add_lastname")->text());
	list.append(Form->findChild<QComboBox*>("lineEdit_add_BAC")->currentText());
	list.append(Form->findChild<QComboBox*>("lineEdit_add_department")->currentText());
	controllerAdd->control(list);

	Form->findChild<QLineEdit*>("lineEdit_add_number")->setText("");
	Form->findChild<QLineEdit*>("lineEdit_add_firstname")->setText("");
	Form->findChild<QLineEdit*>("lineEdit_add_lastname")->setText("");
}

/**
 * @brief supprimer un ou plusieurs eleves de la promo
*/
void ViewForms::remove() {
	QStringList list = Form->findChild<QLineEdit*>("lineEdit_remove_number")->text().split(" ");
	controllerDelete->control(list);
	Form->findChild<QLineEdit*>("lineEdit_remove_number")->setText("");
}
