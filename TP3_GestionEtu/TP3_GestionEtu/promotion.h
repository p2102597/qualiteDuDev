#pragma once
#include"qlist.h"
#include"student.h"
#include "observer.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

/**
 * @brief la promotion est un groupe d'eleve
*/
class Promotion : public Observable
{
private:
	QList<Student> liste;
	QList<Observer*> obs;

public:
	Promotion();
	void loadFileCSV(QString fileName);

	void affiche();

	QList<Student>* getQList() {
		return &liste;
	}

	void addStudent(Student* stu);
	void removeStudent(int i);

	Student* find(int id);

	QWidget* getBarChartView();
	QWidget* getPieChartView();

	virtual void addObserver(Observer* observer) override;
	virtual void removeObserver(Observer* observer) override;
	virtual void notifyObserver() override;
};


