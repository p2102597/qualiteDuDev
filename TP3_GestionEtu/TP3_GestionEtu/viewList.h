#pragma once
#include "promotion.h"
#include <qlistwidget.h>
#include <qdialog.h>
#include "controllers.h"

/**
 * @brief Class to manage the QListWidget
 * @author Adrien Peytavie
*/
class ViewList :  public QObject,public Observer
{
	Q_OBJECT;
private:
	Promotion* promo;
	QListWidget* listWidget;
	Abstract_Controller* controllerDelete;
	
public:
	
	ViewList(Promotion* promo, QListWidget* listWidget);
	virtual void update() override;
public slots:
	void remove();
};

