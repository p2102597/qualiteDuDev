#ifndef WEATHERREPORT_H
#define WEATHERREPORT_H

#include <QDebug>

#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonArray>
#include <QString>

#include "observer.h"

/**
 * @brief The WeatherReport class
 * @author FJa
 */
class WeatherReport : public Observable
{
private:
    QString main,description; //> text report
    double temp,temp_min,temp_max; //> temperatures
    double lon,lat; //> localisation
    QVector<Observer*> obs;
public:
    WeatherReport();

    // getters
    const QString& getMain() const { return main; }
    const QString& getDescription() const {return description;}
    double getTemp() const {return temp;}
    double getTempMin() const {return temp_min;}
    double getTempMax() const {return temp_max;}
    double getLon() const {return lon;}
    double getLat() const {return lat;}

    void setData(QString main, QString desc, double temp, double temp_min, double temp_max, double lon, double lat);

    virtual void addObserver(Observer* observer);
    virtual void removeObserver(Observer* observer);
    virtual void notifyObserver() const;

};

#endif // WEATHERREPORT_H
