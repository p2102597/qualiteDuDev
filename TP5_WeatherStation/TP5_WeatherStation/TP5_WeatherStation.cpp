//#include <unistd.h>
#include <QtNetwork/QNetworkAccessManager>
#include <QByteArray>

#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonArray>

#include "TP5_WeatherStation.h"
#include "ui_TP5_WeatherStation.h"

#include "weatherreport.h"

TP5_WeatherStation::TP5_WeatherStation(DbManager *dbm, QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::TP5_WeatherStationClass)
    , weatherReport (new WeatherReport) // Weather Data class
    , dbmanager (dbm)                   // DB Manager, for Pollution Data
    , netmanager (nullptr)              // NetWork Manager, for http requests
{
    ui->setupUi(this);

    // Weather report View
    reportView = new ViewReport(weatherReport,ui);
    // Pollution Forecast View
    pollutionView = new ViewPollution(dbmanager, ui->groupBox_pollution);

    weatherReport->addObserver(reportView);
    weatherReport->addObserver(pollutionView);

    // netmanager here (or better in initialisation list)  + callback to replyFinished
    netmanager = new QNetworkAccessManager(this);
    pollutionnetmanager = new QNetworkAccessManager(this);
    connect(netmanager, SIGNAL(finished(QNetworkReply*)), this, SLOT(weatherReplyFinished(QNetworkReply*)));
    connect(pollutionnetmanager, SIGNAL(finished(QNetworkReply*)), this, SLOT(pollutionReplyFinished(QNetworkReply*)));

    weatherRequest();
    pollutionRequest();
    // uncomment once observable implemented
    connect(ui->pushButton_weather_request, &QPushButton::pressed, this, &TP5_WeatherStation::weatherRequest);
    connect(ui->pushButton_weather_request, &QPushButton::pressed, this, &TP5_WeatherStation::pollutionRequest);

}

TP5_WeatherStation::~TP5_WeatherStation()
{
    delete ui;
    delete dbmanager;
    if (netmanager != nullptr)
        netmanager->deleteLater();

}

void TP5_WeatherStation::weatherRequest() {
    QUrl url("https://api.openweathermap.org/data/2.5/weather?q=bourg-en-bresse,fr&units=metric&lang=fr&appid=8966c83f85b40d7337e9ad5d6e5092a0");

    QNetworkRequest request;
    request.setUrl(url);
    //--header �Accept: application/json�
    request.setRawHeader("Accept", "application/json");
    //qDebug() << Q_FUNC_INFO << request.url();
    netmanager->get(request);
}

void TP5_WeatherStation::weatherReplyFinished(QNetworkReply* reply)
{
    
    if (reply->error() != QNetworkReply::NoError)
    {
        //Network Error 
        qDebug() << reply->error() << "=>" << reply->errorString();
    }
    else if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() ==
        200)
    {
        QByteArray datas = reply->readAll();
        QJsonDocument jsonResponse = QJsonDocument::fromJson(datas);
        QJsonObject jsonObj = jsonResponse.object();

        QJsonObject mainObj = jsonObj["main"].toObject();
        double temp = mainObj["temp"].toDouble();
        double temp_min = mainObj["temp_min"].toDouble();
        double temp_max = mainObj["temp_max"].toDouble();

        QJsonArray weatherArray = jsonObj["weather"].toArray();
        QString main="";
        QString description="";
        for (auto w : weatherArray) {
            main = main + w.toObject()["main"].toString();
            description = description + w.toObject()["description"].toString();
        }
        
        QJsonObject coordObj = jsonObj["coord"].toObject();
        double lon = coordObj["lon"].toDouble();
        double lat = coordObj["lat"].toDouble();
        weatherReport->setData(main,description,temp,temp_min,temp_max,lon,lat);

        weatherReport->notifyObserver();
    }
    else { 
        qDebug() << "probleme"; 
    }


    
    reply->deleteLater();
}

void TP5_WeatherStation::pollutionRequest()
{
    QUrl url("https://api.openweathermap.org/data/2.5/air_pollution/forecast?lat=46.0398&lon=5.4133&units=metric&lang=fr&appid=8966c83f85b40d7337e9ad5d6e5092a0");

    QNetworkRequest request;
    request.setUrl(url);
    //--header �Accept: application/json�
    request.setRawHeader("Accept", "application/json");
    //qDebug() << Q_FUNC_INFO << request.url();
    pollutionnetmanager->get(request);
}

void TP5_WeatherStation::pollutionReplyFinished(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        //Network Error 
        qDebug() << reply->error() << "=>" << reply->errorString();
    }
    else if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() ==
        200)
    {
        QByteArray datas = reply->readAll();
        QJsonDocument jsonResponse = QJsonDocument::fromJson(datas);
        QJsonObject jsonObj = jsonResponse.object();

        QJsonArray weatherArray = jsonObj["list"].toArray();
        for (auto l : weatherArray) {
            QDateTime localTime = QDateTime::fromSecsSinceEpoch(l.toObject()["dt"].toInt());
            qint64 msdt = localTime.toMSecsSinceEpoch();
            dbmanager->addData(l.toObject()["dt"].toInt(), l.toObject()["main"].toObject()["aqi"].toInt());
        }
        weatherReport->notifyObserver();
    }
}
