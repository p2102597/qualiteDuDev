#include "weatherreport.h"
#include <QDateTime>
#include <QTimeZone>
//#include <unistd.h>

WeatherReport::WeatherReport()
    : temp(0.0),temp_min(0.0),temp_max(0.0),
      lon(0.0),lat(0.0)
{ }

void WeatherReport::setData(QString main, QString desc, double temp, double temp_min, double temp_max, double lon, double lat)
{
    this->main = main;
    this->description = desc;
    this->temp = temp;
    this->temp_min = temp_min;
    this->temp_max= temp_max;
    this->lon = lon;
    this->lat = lat;
}

void WeatherReport::addObserver(Observer * observer)
{
    obs.append(observer);
}

void WeatherReport::removeObserver(Observer* observer)
{
    obs.removeAll(observer);
}

void WeatherReport::notifyObserver() const
{
    for (auto o : obs)
    {
        o->update();
    }
}
