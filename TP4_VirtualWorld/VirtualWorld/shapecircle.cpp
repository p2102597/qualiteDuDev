#include "shape.h"
#include <QGraphicsEllipseItem>

int Shape::current_id; // added

Circle::Circle()
{
	id = current_id++;
	pos = QPointF(-1, -1);
	radius = 0.;
}

Circle::Circle(QPointF p, double r) : radius(r)
{
	id = current_id++;
	pos = p;
}

QGraphicsItem* Circle::getGraphicsItem()
{
	QGraphicsEllipseItem* item = new QGraphicsEllipseItem(pos.x()-radius, pos.y()-radius, radius*2., radius*2.);
	item->setData(0,id);
	item->setBrush(QBrush(color));
	return item;
}


QString Circle::type() 
{
	return "Circle";
}

void Circle::setPos(QPointF newPos)
{
	pos = newPos;
}
