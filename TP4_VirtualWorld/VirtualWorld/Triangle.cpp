#include "shape.h"

Triangle::Triangle()
{
	id = current_id++;
	pos = QPointF(-1, -1);
	base = 0.;
	hauteur = 0.;
}

Triangle::Triangle(QPointF p, double b, double h) : base(b), hauteur(h)
{
	id = current_id++;
	pos = p;
}

QGraphicsItem* Triangle::getGraphicsItem()
{
	QGraphicsItemGroup* Triangle = new QGraphicsItemGroup();
	Triangle->addToGroup(new QGraphicsLineItem(pos.x() - base / 2, pos.y(), pos.x() + base / 2, pos.y()));
	Triangle->addToGroup(new QGraphicsLineItem(pos.x() - base / 2, pos.y(), pos.x(), pos.y()-hauteur));
	Triangle->addToGroup(new QGraphicsLineItem(pos.x(), pos.y()-hauteur, pos.x() + base / 2, pos.y()));
	Triangle->setData(0, id);
	return Triangle;
}

QString Triangle::type()
{
	return QString("Triangle");
}

void Triangle::setPos(QPointF p)
{
	pos = QPointF(p.x(),p.y()+hauteur/2) ;
}


QPointF Triangle::getPos() {
	return QPointF(pos.x(), pos.y() - hauteur / 2 );
}