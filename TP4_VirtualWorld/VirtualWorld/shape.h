#pragma once
#include <QPointF>
#include <QGraphicsItem>
#include <qbrush.h>


/**
 * @brief 
*/
class Shape
{
protected:
	QPointF pos;
public:
	virtual QGraphicsItem* getGraphicsItem() = 0;
	virtual QString type() =0;
	virtual QVector<Shape*> getListe() { return QVector<Shape*>(); };
	virtual void setPos(QPointF) = 0;
	virtual QPointF getPos() { return pos; }
	int id;
	QColor color = QColor(255,255,255);
	static int current_id; // added
};

class Circle : public Shape
{
private:
	

public: 
	double radius;
	Circle();
	Circle(QPointF, double);

    QGraphicsItem* getGraphicsItem() override;
    QString type() override;
	void setPos(QPointF) override;
};

class Square : public Shape
{
private:
	double largeur;

public:

	Square();
	Square(QPointF, double);

	QGraphicsItem* getGraphicsItem() override;
	QString type() override;
	void setPos(QPointF) override;
};

class Rect : public Shape
{
private:
	double largeur,hauteur;

public:

	Rect();
	Rect(QPointF, double, double);

	QGraphicsItem* getGraphicsItem() override;
	QString type() override;
	void setPos(QPointF) override;
};

class Triangle : public Shape
{
private:
	double base, hauteur;
public:
	Triangle();
	Triangle(QPointF, double base , double hauteur);

	QGraphicsItem* getGraphicsItem() override;
	QString type() override;
	void setPos(QPointF) override;
	QPointF getPos() override;
};

class House : public Shape
{
protected:
	double largeur, hauteur;
	QVector<Shape*> Liste;
public:

	House();
	House(QPointF, double);

	virtual QGraphicsItem* getGraphicsItem() override;
	virtual QString type() override;
	virtual void setPos(QPointF) override;
};

class Car : public Shape
{
protected:
	double largeur, hauteur;
	QVector<Shape*> Liste;
public:

	Car();
	Car(QPointF, double);

	virtual QGraphicsItem* getGraphicsItem() override;
	virtual QString type() override;
	virtual void setPos(QPointF) override;
};

class Tree : public Shape
{
protected:
	double largeur, hauteur;
	QVector<Shape*> Liste;
public:

	Tree();
	Tree(QPointF, double);

	virtual QGraphicsItem* getGraphicsItem() override;
	virtual QString type() override;
	virtual void setPos(QPointF) override;
};

class Groupe : public Shape
{
private:
	QVector<Shape*> Liste;
public:

	Groupe();
	Groupe(QVector<Shape*>);

	void addShape(Shape*);
	void removeShape(Shape*);

	QVector<Shape*> getListe() override { return Liste; };
	QGraphicsItem* getGraphicsItem() override;
	QString type() override;
	void setPos(QPointF) override;
};
