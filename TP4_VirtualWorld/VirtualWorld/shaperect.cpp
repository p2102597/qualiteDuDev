#include"shape.h"
#include <QGraphicsRectItem>

Rect::Rect()
{
	id = current_id++;
	pos = QPointF(-1, -1);
	largeur = 0.;
	hauteur = 0.;
}

Rect::Rect(QPointF p, double l, double h) : largeur(l), hauteur(h)
{
	id = current_id++;
	pos = p;
}

QGraphicsItem* Rect::getGraphicsItem()
{
	QGraphicsRectItem* item = new QGraphicsRectItem(pos.x() - largeur / 2, pos.y() - hauteur / 2, largeur, hauteur);
	item->setData(0, id);
	item->setBrush(QBrush(color));
	return item;
}

QString Rect::type()
{
	return "Rectangle";
}

void Rect::setPos(QPointF newPos)
{
	pos = newPos;
}
