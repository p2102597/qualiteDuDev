#include "shape.h"

Tree::Tree()
{
	id = current_id++;
	pos = QPointF(-1, -1);
	largeur = 0.;
	hauteur = 0.;
}

Tree::Tree(QPointF p, double l) : largeur(l)
{
	hauteur = largeur / 5 * 8;
	id = current_id++;
	pos = p;
	Liste.append(new Rect(QPointF(p.x(), p.y() + hauteur / 2 - hauteur / 8 * 2), largeur/5, hauteur / 2));
	Liste.append(new Circle(QPointF(p.x(), p.y() + hauteur / 2 - hauteur / 8 * 4.5), largeur / 5));
	Liste.append(new Circle(QPointF(p.x(), p.y() + hauteur / 2 - hauteur / 8 * 6.5), largeur / 5*1.5));
	Liste.append(new Circle(QPointF(p.x() - largeur / 2 + largeur / 5*1.25, p.y() + hauteur / 2 - hauteur / 8 * 6), largeur / 5));
	Liste.append(new Circle(QPointF(p.x() - largeur / 2 + largeur / 5, p.y() + hauteur / 2 - hauteur / 8 * 4.25), largeur / 5));
	Liste.append(new Circle(QPointF(p.x() - largeur / 2 + largeur / 5*3.75, p.y() + hauteur / 2 - hauteur / 8 * 6), largeur / 5));
	Liste.append(new Circle(QPointF(p.x() - largeur / 2 + largeur / 5*4, p.y() + hauteur / 2 - hauteur / 8 * 4.25), largeur / 5));
}

QGraphicsItem* Tree::getGraphicsItem()
{
	QGraphicsItemGroup* item = new QGraphicsItemGroup();
	for (auto q : Liste) {
		item->addToGroup(q->getGraphicsItem());
	}
	item->setData(0, id);
	return item;
}

QString Tree::type()
{
	return QString("Tree");
}

void Tree::setPos(QPointF newPos)
{
	QPointF diffPos = newPos - pos;
	pos = newPos;
	for (Shape*& sh : Liste) {
		sh->setPos(sh->getPos() + diffPos);
	}
}
