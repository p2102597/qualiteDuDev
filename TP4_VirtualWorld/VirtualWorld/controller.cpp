#include "controller.h"
#include "shapemanager.h"

#include <cstdlib>
#include <iostream>
#include <ctime>

ControllerAdd::ControllerAdd(ShapeManager* sm) : shapeManager(sm)
{
}

void ControllerAdd::control(QString name)
{
	Shape* forme = ShapeFactory::getShape(name);
	if (forme != nullptr) {
		shapeManager->add(forme);
	}
	shapeManager->notifyObserver();
}


ControllerMoveShape::ControllerMoveShape(ShapeManager* sm) : shapeManager(sm)
{}

void ControllerMoveShape::control(const QVector<QGraphicsItem*>& items)
{
	if (shapeManager == nullptr) return;

	// Move Shape
	for (QGraphicsItem* item : items)
	{
		int id = item->data(0).toInt();


		bool selected = shapeManager->selectShape(id);
		if (selected)
		{
			QRectF rect = item->boundingRect();
			
			shapeManager->moveShape(item->scenePos() + rect.center()); // erreur ici pour deplacment des groupes
		}
	}
}

ControllerRemove::ControllerRemove(ShapeManager* sm) : shapeManager(sm)
{
}

void ControllerRemove::control(QVector<int> id)
{
	shapeManager->removeShape(id);
}

ControllerGroupe::ControllerGroupe(ShapeManager* sm) : shapeManager(sm)
{
}

void ControllerGroupe::control(const QGraphicsItem* groupeSelected)
{
	if (shapeManager == nullptr or groupeSelected->childItems().length() == 0) return;
	QVector<Shape*> final;
	QVector<Shape*> allShape = shapeManager->getShapes();
	for (Shape* sh : allShape) {
		for (auto QG : groupeSelected->childItems()) {
			if (sh->id == QG->data(0)) {
				final.append(sh);
				QVector<int> id;
				id.append(sh->id);
				shapeManager->removeShape(id);
			}
		}
	}
	shapeManager->add(new Groupe(final));
}

ControllerColor::ControllerColor(ShapeManager* sm) : shapeManager(sm)
{
}

void ControllerColor::control(QColor Color, QVector<int> id)
{
	shapeManager->changeColor(Color, id);
}
