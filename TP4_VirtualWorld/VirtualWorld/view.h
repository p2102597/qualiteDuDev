#pragma once
#include "shapemanager.h"
#include "controller.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QTreeWidget>
#include <QKeyEvent>


class PaintView : public QGraphicsScene, public Observer//: public QObject
{
	Q_OBJECT
private:
	ShapeManager* shapeManager;
	QVector<QGraphicsItem*> selected;
	QGraphicsItem* groupeSelected;
	QVector<QGraphicsItem*> tools;
	QPointF mousePos;
	QString toolbox;

	bool grouped = false;

public:
	PaintView(ShapeManager* = nullptr);
	QGraphicsItem* getGroupeSelected();
	void setGroupeSelected(QGraphicsItem*);
	void updateModel();

protected:

    void drawForeground(QPainter* painter, const QRectF& rect);


public slots:
	void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent);
	void mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent);
	void keyPressEvent(QKeyEvent* event);
	void keyReleaseEvent(QKeyEvent* event);
};

class TreeView : public Observer
{
private:
	ShapeManager* shapeManager;
	QTreeWidget* treeview;

public:
	TreeView(ShapeManager* = nullptr, QTreeWidget* = nullptr);

	void updateModel();
	void recursive(QVector<Shape*>,QTreeWidgetItem*);
	void recursive(QVector<Shape*>, QTreeWidget*);
};
