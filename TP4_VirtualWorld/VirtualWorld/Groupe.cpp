#include"shape.h"

Groupe::Groupe()
{
	id = current_id++;
}

Groupe::Groupe(QVector<Shape*> l )
{
	Liste.append(l);
	id = current_id++;
	QRectF rect = getGraphicsItem()->boundingRect();
	pos = rect.center();
}

void Groupe::addShape(Shape* l)
{
	Liste.append(l);
	QRectF rect = getGraphicsItem()->boundingRect();
	pos = rect.center();
}

void Groupe::removeShape(Shape* l)
{
	Liste.removeAll(l);
}

QGraphicsItem* Groupe::getGraphicsItem()
{
	QGraphicsItemGroup* item = new QGraphicsItemGroup();
	for (auto q : Liste) {
		item->addToGroup(q->getGraphicsItem());
	}
	item->setData(0, id);
	return item;
}

QString Groupe::type()
{
	return "Groupe";
}

void Groupe::setPos(QPointF newPos)
{
	QPointF diffPos = newPos - pos;

	pos = newPos;
	for (Shape*& sh : Liste) {
		sh->setPos(sh->getPos() + diffPos);
	}
}
