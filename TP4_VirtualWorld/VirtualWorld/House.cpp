#include "shape.h"

House::House()
{
	id = current_id++;
	pos = QPointF(-1, -1);
	largeur = 0.;
	hauteur = 0.;
}

House::House(QPointF p, double l) : largeur(l)
{
	hauteur = largeur / 2 * 3;
	id = current_id++;
	pos = p;
	Liste.append(new Square(QPointF(p.x(), p.y() + hauteur / 2 - hauteur / 3), l));
	Liste.append(new Triangle(QPointF(p.x(), p.y() + hauteur / 2 - hauteur / 3 * 2), l, hauteur / 3));
	Liste.append(new Rect(QPointF(p.x(), p.y() + hauteur / 2 - hauteur / 3 * 2 / 15 * 3), l / 15 * 3, hauteur / 3 * 2 / 15 * 6));
	Liste.append(new Circle(p, l / 15 * 3));
	Liste.append(new Square(QPointF(p.x()-l/2+l/15*3, p.y() + hauteur / 2 - hauteur /3*2/15*5),l/15*4));
	Liste.append(new Square(QPointF(p.x() - l / 2 + l / 15 * 12, p.y() + hauteur / 2 - hauteur / 3 * 2 / 15 * 5), l / 15 * 4));
}

QGraphicsItem* House::getGraphicsItem()
{
	QGraphicsItemGroup* item = new QGraphicsItemGroup();
	for (auto q : Liste) {
		item->addToGroup(q->getGraphicsItem());
	}
	item->setData(0, id);
	return item;
}

QString House::type()
{
	return QString("House");
}

void House::setPos(QPointF newPos)
{
	QPointF diffPos = newPos - pos;
	pos = newPos;
	for (Shape*& sh : Liste) {
		sh->setPos(sh->getPos() + diffPos);
	}
}
