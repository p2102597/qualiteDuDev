#include"shape.h"

Car::Car()
{
	id = current_id++;
	pos = QPointF(-1, -1);
	largeur = 0.;
	hauteur = 0.;
}

Car::Car(QPointF p, double l) : largeur(l)
{
	hauteur = largeur / 7 * 4;
	id = current_id++;
	pos = p;
	Liste.append(new Rect(QPointF(p.x(),p.y()+ hauteur/2-hauteur/4*1.5),largeur,hauteur/4*2));
	Liste.append(new Rect(QPointF(p.x(), p.y() + hauteur / 2 - hauteur/4* 3.25), largeur/7*4, hauteur / 4 * 1.5));
	Liste.append(new Circle(QPointF(p.x()-largeur/2+largeur/7*1.5, p.y() + hauteur / 2 - hauteur / 4 * 0.5), largeur / 7));
	Liste.append(new Circle(QPointF(p.x() - largeur / 2 + largeur / 7 *5.5, p.y() + hauteur / 2 - hauteur / 4 * 0.5), largeur / 7));
}

QGraphicsItem* Car::getGraphicsItem()
{
	QGraphicsItemGroup* item = new QGraphicsItemGroup();
	for (auto q : Liste) {
		item->addToGroup(q->getGraphicsItem());
	}

	item->setData(0, id);
	return item;
}

QString Car::type()
{
	return QString("Car");
}

void Car::setPos(QPointF newPos)
{
	QPointF diffPos = newPos - pos;
	pos = newPos;
	for (Shape*& sh : Liste) {
		sh->setPos(sh->getPos() + diffPos);
	}
}

