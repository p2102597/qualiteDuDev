#include "shapemanager.h"


ShapeManager::ShapeManager() : Observable(), selected(nullptr)
{}

void ShapeManager::add(Shape* shape)
{
	if (shape == nullptr) return;

	shapes.append(shape);
	notifyObserver();
}


void ShapeManager::moveShape(QPointF pos)
{
	if (selected == nullptr) return;

	selected->setPos(pos);
	notifyObserver();
}

void ShapeManager::removeShape(QVector<int> id)
{
	int idR;
	int cpt = 0;
	QVector<int> indexARemove;
	for (Shape* sh : shapes) {
		for (auto index : id) {
			if (sh->id == index) {
				indexARemove.insert(0, cpt);
				break;
			}
		}
		cpt++;
	}
	cpt = 0;
	for (auto index : indexARemove) {
		shapes.remove(index);
		cpt++;
	}

	notifyObserver();
}

void ShapeManager::changeColor(QColor color, QVector<int> id)
{
	for (Shape* sh : shapes) {
		for (auto index : id) {
			if (sh->id == index) {
				sh->color = color;
				
			}
		}
	}
}

bool ShapeManager::selectShape(int id)
{
	selected = nullptr;
	for (Shape* shape : shapes)
	{
		if (shape->id == id)
		{
			selected = shape;
			return true;
		}
	}

	return false;
}

const Shape* ShapeManager::getShape(int i) const
{
	for (auto sh : shapes) {
		if (sh->id == i) return sh;
	}
}
