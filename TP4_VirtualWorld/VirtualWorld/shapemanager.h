#pragma once

#include "shape.h"
#include "observer.h"
#include <QVector>
#include <QPointF>
#include <qset.h>

class ShapeManager : public Observable
{
private:
	QVector<Shape*> shapes;
	Shape* selected;

public:
	ShapeManager();
	void add(Shape*);
	void moveShape(QPointF);
	void removeShape(QVector<int>);
	void changeColor(QColor, QVector<int>);

	bool selectShape(int);

    const QVector<Shape*>& getShapes() const {return shapes;}
	const Shape* getShape(int i) const;
};

