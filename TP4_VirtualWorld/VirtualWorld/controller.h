#pragma once
#include "shapemanager.h"
#include "ShapeFactory.h"
#include "shape.h"
#include <QPointF>

class ControllerAdd
{
	ShapeManager* shapeManager;
public:
	
	ControllerAdd(ShapeManager* = nullptr);
	void control(QString);
};

class ControllerRemove
{
	ShapeManager* shapeManager;

public:
	ControllerRemove(ShapeManager* = nullptr);
	void control(QVector<int>);
};

class ControllerMoveShape
{
	ShapeManager* shapeManager;
public:
	ControllerMoveShape(ShapeManager* = nullptr);
    void control(const QVector<QGraphicsItem *> &);
};

class ControllerGroupe
{
	ShapeManager* shapeManager;

public:
	ControllerGroupe(ShapeManager* = nullptr);
	void control(const QGraphicsItem* groupeSelected);
};

class ControllerColor
{
	ShapeManager* shapeManager;

public:
	ControllerColor(ShapeManager* = nullptr);
	void control(QColor Color, QVector<int>);
};
