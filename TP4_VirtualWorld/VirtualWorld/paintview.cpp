#include "view.h"
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>

PaintView::PaintView( ShapeManager* sm) : QGraphicsScene(), Observer(), shapeManager(sm)
{
	groupeSelected = new QGraphicsItemGroup();
}

QGraphicsItem* PaintView::getGroupeSelected()
{
	return groupeSelected;
}

void PaintView::setGroupeSelected(QGraphicsItem* g)
{
	groupeSelected = g;
}

void PaintView::drawForeground(QPainter* painter, const QRectF& rect)
{
	if (toolbox.isEmpty()) return;

	QGraphicsView* gv = (QGraphicsView*)this->parent();
	
	QPointF p1 = gv->mapToScene(QPoint(10, 10));
	
	painter->save();
	
	painter->setBrush(QBrush(QColor(229,255,204)));
	painter->setPen(Qt::black);
	
	painter->drawRect(p1.x() + 10, p1.y() + 10, toolbox.size() * 7, 20);
	painter->drawText(int(p1.x() + 14), int(p1.y() + 12), toolbox.size() * 10, 20, Qt::AlignLeft, toolbox);

	painter->restore();

	QRectF r = groupeSelected->boundingRect();

	if (r.width() != 0) {
		painter->drawRect(r.x() - 5, r.y() - 5, r.width() + 10, r.height() + 10);
	}
}

void PaintView::updateModel()
{
	// Clear
	clear();

	// Get models
	QVector<Shape*> shapes = shapeManager->getShapes();

	for (Shape* shape : shapes)
	{
		QGraphicsItem* item = shape->getGraphicsItem();
		item->setAcceptDrops(true);

		addItem(item);
	}
}

void PaintView::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	if (mouseEvent->button() == Qt::LeftButton)
	{
		
		mousePos = mouseEvent->scenePos();
		toolbox = "mousePressEvent (" + QString::number(mousePos.x()) + "," + QString::number(mousePos.y());

		selected = this->items(mousePos);


		if (grouped) {
			if (!selected.isEmpty()) {
				QGraphicsItemGroup* item = new QGraphicsItemGroup();
				QVector<QGraphicsItem*> oldSelected = groupeSelected->childItems();
				for (QGraphicsItem* QT : oldSelected) {
					item->addToGroup(QT);
				}
				for (QGraphicsItem* QT : selected) {
					item->addToGroup(QT);
				}
				groupeSelected = item;
			}
		}
		else {
			groupeSelected = new QGraphicsItemGroup();
		}
		update();
	}
	
}

void PaintView::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	if ( selected.size() > 0 && (mouseEvent->buttons() & Qt::LeftButton))
	{

		QVector<Shape*> realShapes = shapeManager->getShapes();

		QPointF mousePosNew = mouseEvent->scenePos();

		toolbox = "mouseMoveEvent (" + QString::number(mousePosNew.x()) + "," + QString::number(mousePosNew.y());
		QPointF mouseD = mousePosNew - mousePos;
		for (QGraphicsItem* item : selected)
		{
			for (auto real : realShapes) {
				if (item->data(0)==real->id) {
					item->moveBy(mouseD.x(), mouseD.y());
				}
			}
			
		}
		mousePos = mousePosNew;
	}
}

void PaintView::mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	toolbox = "mouseReleaseEvent";

	// Call Controller to modify the model
	(new ControllerMoveShape(shapeManager))->control(selected);
	selected.clear();

	
}
void PaintView::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Shift)
	{
		grouped = true;
	}
}

void PaintView::keyReleaseEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Shift)
	{
		grouped = false;
	}
}