#include "ShapeFactory.h"


Shape* ShapeFactory::getShape(QString forme)
{
	if (forme == "Circle") {
		return new Circle(QPointF(std::rand() % 400 - 200, std::rand() % 400 - 200), std::rand() % 100);
	}
	else if (forme == "Rectangle") {
		return new Rect(QPointF(std::rand() % 400 - 200, std::rand() % 400 - 200), std::rand() % 100, std::rand() % 100);
	}
	else if (forme == "Square") {
		return new Square(QPointF(std::rand() % 400 - 200, std::rand() % 400 - 200), std::rand() % 100);
	}
	else if (forme == "Triangle") {
		return new Triangle(QPointF(std::rand() % 400 - 200, std::rand() % 400 - 200),20,20);
	}
	else if (forme == "House") {
		return new House(QPointF(std::rand() % 400 - 200, std::rand() % 400 - 200), 40);
	}
	else if (forme == "Car") {
		return new Car(QPointF(std::rand() % 400 - 200, std::rand() % 400 - 200), 100);
	}
	else if (forme == "Tree") {
		return new Tree(QPointF(std::rand() % 400 - 200, std::rand() % 400 - 200), 100);
	}
	return nullptr;
}
