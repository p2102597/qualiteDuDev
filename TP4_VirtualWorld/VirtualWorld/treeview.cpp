#include "view.h"

TreeView::TreeView(ShapeManager* sm, QTreeWidget* tw) : Observer(), shapeManager(sm), treeview(tw)
{}

void TreeView::updateModel()
{
	// Clear Treeview
	treeview->clear();

	// Generate Data
	QStringList labels;
	labels << "id" << "type";
	treeview->setHeaderLabels(labels);

	QVector<Shape*> shapes = shapeManager->getShapes();
	recursive(shapes, treeview);
	
}

void TreeView::recursive(QVector<Shape*> shape, QTreeWidgetItem* item)
{
	for (Shape* s : shape)
	{
		QTreeWidgetItem* item2 = new QTreeWidgetItem(item);
		item2->setText(0, QString::number(s->id));
		item2->setText(1, s->type());
		if (s->type() == "Groupe") {
			recursive(s->getListe(), item2);
		}
	}
}

void TreeView::recursive(QVector<Shape*> shape, QTreeWidget* item)
{
	for (Shape* s : shape)
	{
		QTreeWidgetItem* item2 = new QTreeWidgetItem(item);
		item2->setText(0, QString::number(s->id));
		item2->setText(1, s->type());
		if (s->type() == "Groupe") {
			recursive(s->getListe(), item2);
		}
	}
}


