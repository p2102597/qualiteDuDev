#include "virtualworld.h"
#include "controller.h"
#include <QDebug>
#include <qcolordialog.h>

VirtualWorld::VirtualWorld(QWidget *parent, ShapeManager* sm)
    : QMainWindow(parent), shapeManager(sm)
{
    ui.setupUi(this);

    // Paint View
    paintview = new PaintView(shapeManager);
    shapeManager->addObserver(paintview);
    ui.graphicsView->setScene(paintview);
    paintview->setParent(ui.graphicsView);

    // Tree View
    treeview = new TreeView(shapeManager, ui.treeWidget);
    shapeManager->addObserver(treeview);

    controllerAdd = new ControllerAdd(shapeManager);
    controllerRemove = new ControllerRemove(shapeManager);
    controllerGroupe = new ControllerGroupe(shapeManager);
    controllerColor = new ControllerColor(shapeManager);

    // Connect buttons
    connect(ui.Object_AddButton, &QPushButton::pressed, this, &VirtualWorld::addShape);
    connect(ui.Object_RemoveButton, &QPushButton::pressed, this, &VirtualWorld::removeShape);
    connect(ui.Object_GroupButton, &QPushButton::pressed, this, &VirtualWorld::groupeShape);
    connect(ui.Object_ColorButton, &QPushButton::pressed, this, &VirtualWorld::ColorShape);
}

VirtualWorld::~VirtualWorld()
{}

void VirtualWorld::removeShape()
{
    QVector<int> id;
    if (ui.treeWidget->currentItem() != NULL) {
        id.append(ui.treeWidget->currentItem()->text(0).toInt());
    }
    else {
        for (auto QG : paintview->getGroupeSelected()->childItems()) {
            int tmp = QG->data(0).toInt();
            id.append(tmp);
        }
    }
    controllerRemove->control(id);
}

void VirtualWorld::addShape()
{
    QList<QRadioButton*> maListe;
    maListe.append(ui.widget_2->findChild<QRadioButton*>("radioButton_Circle"));
    maListe.append(ui.widget_2->findChild<QRadioButton*>("radioButton_Rectangle"));
    maListe.append(ui.widget_2->findChild<QRadioButton*>("radioButton_Square"));
    maListe.append(ui.widget_2->findChild<QRadioButton*>("radioButton_Triangle")); 
    maListe.append(ui.widget_2->findChild<QRadioButton*>("radioButton_House"));
    maListe.append(ui.widget_2->findChild<QRadioButton*>("radioButton_Car")); 
    maListe.append(ui.widget_2->findChild<QRadioButton*>("radioButton_Tree"));
    for (QRadioButton* qR : maListe) {
        if (qR->isChecked()) {
            controllerAdd->control(qR->text());
        }
    }
    
    
}

void VirtualWorld::groupeShape() {
    controllerGroupe->control(paintview->getGroupeSelected());
}

void VirtualWorld::ColorShape()
{
    QVector<int> id;
    for (auto QG : paintview->getGroupeSelected()->childItems()) {
        int tmp = QG->data(0).toInt();
        id.append(tmp);
    }

    QColor col = QColorDialog::getColor(QColor(255, 0, 0), this);
    controllerColor->control(col, id);

    paintview->updateModel();
}
