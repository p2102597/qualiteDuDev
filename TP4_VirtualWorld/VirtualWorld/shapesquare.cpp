#include "shape.h"
#include <QGraphicsRectItem>

Square::Square()
{
	id = current_id++;
	pos = QPointF(-1, -1);
	largeur = 0.;
}

Square::Square(QPointF p, double l): largeur(l)
{
		id = current_id++;
		pos = p;
}


QGraphicsItem* Square::getGraphicsItem()
{
	QGraphicsRectItem* item = new QGraphicsRectItem(pos.x() - largeur / 2, pos.y() - largeur / 2, largeur, largeur);
	item->setData(0, id);
	item->setBrush(QBrush(color));
	return item;
}

QString Square::type()
{
	return "square";
}

void Square::setPos(QPointF newPos)
{
	pos = newPos;
}
